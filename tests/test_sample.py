"""Sample unit tests"""
import importlib
import pkgutil

import pytest

import ur48483a_pid_controller


def test_pass():
    from ur48483a_pid_controller.hello import hello

    hello()


@pytest.mark.parametrize(
    "module_name",
    [
        name
        for _, name, _ in pkgutil.walk_packages(ur48483a_pid_controller.__path__, ur48483a_pid_controller.__name__ + ".")
    ],
)
def test_import_all_modules(module_name):
    importlib.import_module(module_name)


@pytest.mark.slow
def test_slow():
    print("This is a very slow test which will sometimes be skipped (see the readme)")
    assert True
