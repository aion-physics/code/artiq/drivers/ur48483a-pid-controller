"""
Version bootstrapping, inspired and heavily borrowed from versioneer.

2022 CFAB

This module reads two things to make a version string:

1. The current git hash (and the dirty / clean state of the working directory)
2. The version specified by the file "VERSION.json" in the root of the package.

The version will be the contents of VERSION.json with the git hash appended, e.g.

    1.2.3+abc123

or, if the working directory is dirty,

    1.2.3+abc123.d

In addition, this module intercepts setuptools builds and hard-codes the current
version into them via thee file _VERSIONFILE_SRC.
"""

_VERSIONFILE_SRC = "ur48483a_pid_controller/_version.py"


import os
import sys


from ur48483a_pid_controller._version import get_version


SHORT_VERSION_PY = """
# This file was generated by 'package_versioning.py' from this project's
# git history and version marker file. Distribution tarballs contain a
# pre-generated copy of this file.

version = "%s"


def get_version():
    return version
"""


def write_to_version_file(filename, version):
    """Write the given version number to the given _version.py file."""
    os.unlink(filename)
    with open(filename, "w") as f:
        f.write(SHORT_VERSION_PY % version)

    print("set %s to '%s'" % (filename, version))


def get_cmdclass(cmdclass=None):
    """Get the custom setuptools/distutils subclasses used by Versioneer.

    If the package uses a different cmdclass (e.g. one from numpy), it
    should be provide as an argument.
    """
    if "versioneer" in sys.modules:
        del sys.modules["versioneer"]
        # this fixes the "python setup.py develop" case (also 'install' and
        # 'easy_install .'), in which subdependencies of the main project are
        # built (using setup.py bdist_egg) in the same python process. Assume
        # a main project A and a dependency B, which use different versions
        # of Versioneer. A's setup.py imports A's Versioneer, leaving it in
        # sys.modules by the time B's setup.py is executed, causing B to run
        # with the wrong versioneer. Setuptools wraps the sub-dep builds in a
        # sandbox that restores sys.modules to it's pre-build state, so the
        # parent is protected against the child's "import versioneer". By
        # removing ourselves from sys.modules here, before the child build
        # happens, we protect the child from the parent's versioneer too.
        # Also see https://github.com/python-versioneer/python-versioneer/issues/52

    cmds = {} if cmdclass is None else cmdclass.copy()

    # we override "build_py" in both distutils and setuptools
    #
    # most invocation pathways end up running build_py:
    #  distutils/build -> build_py
    #  distutils/install -> distutils/build ->..
    #  setuptools/bdist_wheel -> distutils/install ->..
    #  setuptools/bdist_egg -> distutils/install_lib -> build_py
    #  setuptools/install -> bdist_egg ->..
    #  setuptools/develop -> ?
    #  pip install:
    #   copies source tree to a tempdir before running egg_info/etc
    #   if .git isn't copied too, 'git describe' will fail
    #   then does setup.py bdist_wheel, or sometimes setup.py install
    #  setup.py egg_info -> ?

    # we override different "build_py" commands for both environments
    if "build_py" in cmds:
        _build_py = cmds["build_py"]
    elif "setuptools" in sys.modules:
        from setuptools.command.build_py import build_py as _build_py
    else:
        from distutils.command.build_py import build_py as _build_py

    class cmd_build_py(_build_py):
        def run(self):
            version = get_version()

            # unless we update this, the command will keep using the old
            # version
            self.distribution.metadata.version = version

            _build_py.run(self)
            # now locate _version.py in the new build/ directory and replace
            # it with an updated value
            target_versionfile = os.path.join(self.build_lib, _VERSIONFILE_SRC)
            print("UPDATING %s" % target_versionfile)
            write_to_version_file(target_versionfile, version)

    cmds["build_py"] = cmd_build_py

    if "setuptools" in sys.modules:
        from setuptools.command.sdist import sdist as _sdist
    else:
        from distutils.command.sdist import sdist as _sdist

    class cmd_sdist(_sdist):
        def run(self):
            version = get_version()
            self._versioneer_generated_version = version
            # unless we update this, the command will keep using the old
            # version
            self.distribution.metadata.version = version
            return _sdist.run(self)

        def make_release_tree(self, base_dir, files):
            _sdist.make_release_tree(self, base_dir, files)
            # now locate _version.py in the new base_dir directory
            # (remembering that it may be a hardlink) and replace it with an
            # updated value
            target_versionfile = os.path.join(base_dir, _VERSIONFILE_SRC)
            print("UPDATING %s" % target_versionfile)
            write_to_version_file(
                target_versionfile, self._versioneer_generated_version
            )

    cmds["sdist"] = cmd_sdist

    return cmds
