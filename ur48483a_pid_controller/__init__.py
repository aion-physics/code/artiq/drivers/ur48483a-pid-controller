"""ur48483a_pid_controller - A Python package to send commands to and edit settings of a PID controller to use with ARTIQ."""

__author__ = "David Evans <de121@ic.ac.uk>"
__all__ = []

from ._version import get_version

__version__ = get_version()
del get_version
